#!/bin/bash
http POST localhost:8111/api/v1/techs \
	name="Python" \
	description="Python is an interpreted, high-level and general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace. " \
	url="https://www.python.org/"\
	img="static/python.png"

http POST localhost:8111/api/v1/techs \
	name="Javascript" \
	description="JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification. JavaScript is high-level, often just-in-time compiled, and multi-paradigm. It has curly-bracket syntax, dynamic typing, prototype-based object-orientation, and first-class functions." \
	url="https://www.javascript.com/"\
	img="static/js.png"

http POST localhost:8111/api/v1/techs \
	name="Rust" \
	description="Rust is a multi-paradigm programming language focused on performance and safety, especially safe concurrency. Rust is syntactically similar to C++, and provides memory safety without using garbage collection, but instead through the use of a borrow checking system." \
	url="https://www.rust-lang.org/"\
	img="static/rust.png"

http POST localhost:8111/api/v1/techs \
	name="Go" \
	description=" Go is an open source programming language that makes it easy to build simple, reliable, and efficient software." \
	url="https://golang.org/"\
	img="static/go.png"

http POST localhost:8111/api/v1/techs \
	name="Docker" \
	description="Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels." \
	url="https://www.docker.com/"\
	img="static/docker.png"

http POST localhost:8111/api/v1/techs \
	name="Linux" \
	description="Linux is a family of open-source Unix-like operating systems based on the Linux kernel, an operating system kernel first released on September 17, 1991, by Linus Torvalds. Linux is typically packaged in a Linux distribution." \
	url="https://www.linux.org/"\
	img="static/tux.png"
