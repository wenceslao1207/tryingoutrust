-- Your SQL goes here

CREATE TABLE tags (
	id SERIAL PRIMARY KEY,
	title VARCHAR NOT NULL
);

CREATE TABLE tag_posts (
	tag_id INT REFERENCES tags (id) ON UPDATE CASCADE ON DELETE CASCADE,
	posts_id INT REFERENCES posts (id) ON UPDATE CASCADE ON DELETE CASCADE,
   	CONSTRAINT tag_posts_pkey PRIMARY KEY (tag_id, posts_id)  -- explicit pk
);

ALTER TABLE posts
ADD COLUMN tag VARCHAR;
