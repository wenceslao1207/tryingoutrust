-- Your SQL goes here

CREATE TABLE technologies (
	id SERIAL PRIMARY KEY,
	name VARCHAR NOT NULL,
	description TEXT NOT NULL,
	url VARCHAR,
	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
	deleted_at TIMESTAMP
)
