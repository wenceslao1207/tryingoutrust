use rocket::http::Method;
use rocket_cors::{AllowedHeaders, AllowedOrigins, Cors};


pub fn cors() -> Cors {
    let _allowed_origins = AllowedOrigins::some_exact(&["http://localhost:3000"]);
    let _allowed_headers = &[
        "Authorization",
        "Accept",
        "Accept-Encoding",
        "Accept-Language",
        "Access-Control-Request-Headers",
        "Access-Control-Request-Methods",
        "Connection",
        "Host",
        "Origin",
        "Referer",
        "User_Agent",
    ];

    // You can also deserialize this
    let cors = rocket_cors::CorsOptions {
        allowed_origins: AllowedOrigins::all(),
        allowed_methods: vec![Method::Get,Method::Post, Method::Options]
            .into_iter()
            .map(From::from)
            .collect(),
        allowed_headers: AllowedHeaders::all(),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors();

    match cors {
        Ok(cors) => cors,
        Err(err) => {
            println!("{}", err);
            panic!(err);
        },
    }
}
