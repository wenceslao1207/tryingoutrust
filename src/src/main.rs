#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] 
extern crate diesel;
extern crate dotenv;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate rocket;
extern crate serde_derive;
#[macro_use]
extern crate rocket_contrib;
extern crate lettre;

use dotenv::dotenv;
use std::env;
use routes::*;
use rocket::*;

mod models;
mod db;
mod schema;
mod routes;
mod controllers;
mod auth;
mod cors;
mod mail;


fn rocket() -> rocket::Rocket {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");

    let cors = cors::cors(); // CORS!

    let pool = db::database::init_pool(database_url);
    rocket::ignite()
        .manage(pool)
        .attach(cors)
        .mount(
            "/api/v1/",
            routes![
                get_all,
                find_user,
                find_user_by_username,
                new_user,
                login,
                get_all_posts,
                get_post_by_title,
                new_post,
                get_all_techs,
                new_tech,
                delete_tech,
            ],
        )
        .mount(
            "/",
            routes![
                static_content,
            ]
        )
}

fn main() {
    mail::mail::send_email("wenceslao1207@protonmail.com".to_string(), "askjdha".to_string(), "aksjdh".to_string());
    rocket().launch();
}
