use diesel;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use super::super::schema::technologies;
use super::super::schema::technologies::dsl::technologies as all_techs;
use super::super::db::pagination;
use serde::{Serialize, Deserialize};
use chrono::NaiveDateTime;

#[derive(Queryable, Serialize)]
pub struct Technology {
    pub id: i32,
    pub name: String,
    pub description: String,
    pub url: Option<String>,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
    pub deleted_at: Option<NaiveDateTime>,
    pub img: Option<String>,
}

#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "technologies"]
pub struct NewTechnology {
    pub name: String,
    pub description: String,
    pub url: Option<String>,
    pub img: Option<String>,
}

impl Technology {
    pub fn get_all_techs(conn: &PgConnection) -> Vec<Technology> {
        all_techs
            .order(technologies::id.desc())
            .load::<Technology>(conn)
            .expect("error")
    }

    pub fn insert_tech(tech: NewTechnology, conn: &PgConnection) -> bool {
        diesel::insert_into(technologies::table)
            .values(&tech)
            .execute(conn)
            .is_ok()
    }

    pub fn delete_tech(id: i32, conn: &PgConnection) -> usize {
        diesel::delete(all_techs.filter(technologies::id.eq(id)))
            .execute(conn)
            .expect("Error deleting technologies")
    }
}
