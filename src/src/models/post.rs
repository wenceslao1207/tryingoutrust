use diesel;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use super::super::schema::posts;
use super::super::schema::posts::dsl::posts as all_posts;
use super::super::db::pagination;
use serde::{Serialize, Deserialize};
use chrono::NaiveDateTime;

#[derive(Queryable, Serialize)] 
pub struct Post {
    pub id: i32,
    pub title: String,
    pub body: String,
    pub published: bool,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
    pub deleted_at: Option<NaiveDateTime>,
    pub tag: Option<String>,
}

#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "posts"]
pub struct NewPost {
    pub title: String,
    pub body: String,
    pub published: bool,
}

impl Post {
    pub fn get_all_posts(conn: &PgConnection) -> Vec<Post> {
        all_posts
            .order(posts::id.desc())
            .load::<Post>(conn)
            .expect("error")
    }

    pub fn insert_post(post: NewPost, conn: &PgConnection) -> bool {
        diesel::insert_into(posts::table)
            .values(&post)
            .execute(conn)
            .is_ok()
    }
    
    pub fn get_post_by_title(title: String, conn: &PgConnection) -> Vec<Post> {
        all_posts
            .filter(posts::title.eq(title))
            .load::<Post>(conn)
            .expect("error")
    }
}

