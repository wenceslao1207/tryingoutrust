use super::super::schema::users;
use super::super::schema::users::dsl::users as all_users;
use chrono::NaiveDateTime;
use diesel;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use pwhash::bcrypt;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::fmt;

const EMAIL_REGEX: &str =
    r"^([a-z0-9_+]([a-z0-9_+.]*[a-z0-9_+])?)@([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6})";
const PASS_REGEX: &str = r"^[A-Za-z0-9](\.?[a-z0-9])*";

type Result<T> = std::result::Result<T, Box<dyn Error>>;

#[derive(Debug, Clone)]
pub struct InvalidEmailError;

#[derive(Debug, Clone)]
pub struct InvalidPasswordError;

#[derive(Debug, Clone)]
pub struct InvalidUserError;

#[derive(Debug, Clone)]
pub struct SamePasswordError;

#[derive(Debug, Clone)]
pub struct SameEmailError;

#[derive(Debug, Clone)]
pub struct SameUsernameError;

#[derive(Queryable, Serialize)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub email: String,
    pub password: String,
    pub active: bool,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
    pub deleted_at: Option<NaiveDateTime>,
}

#[derive(Deserialize)]
pub struct UserData {
    pub username: String,
}

#[derive(Serialize)]
pub struct UserDisplay {
    pub id: i32,
    pub username: String,
    pub email: String,
    pub active: bool,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[derive(Serialize, Deserialize, Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub username: String,
    pub email: String,
    pub password: String,
}

// Error implementation for both errors.
impl Error for InvalidEmailError {}
impl Error for InvalidPasswordError {}
impl Error for SameEmailError {}
impl Error for SamePasswordError {}
impl Error for SameUsernameError {}

// Implementation for the InvalidEmailError.
impl fmt::Display for InvalidEmailError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid email address")
    }
}

// Implementation for the InvalidPasswordError.
impl fmt::Display for InvalidPasswordError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Invalid password")
    }
}

// Implementation for the InvalidEmailError.
impl fmt::Display for SameEmailError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Email already in use")
    }
}

// Implementation for the InvalidPasswordError.
impl fmt::Display for SamePasswordError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Same password")
    }
}

// Implementation for the InvalidPasswordError.
impl fmt::Display for SameUsernameError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Username already in use")
    }
}

pub trait Display {
    type Output;

    fn display(&self) -> Self::Output;
}

pub trait UserValidation {
    fn validate_password(&self, pass: String) -> Result<()>;
    fn validate_email(&self, email: String) -> Result<()>;
    fn validate(&self) -> Result<()>;
}

impl Display for Vec<User> {
    type Output = Vec<UserDisplay>;

    fn display(&self) -> Vec<UserDisplay> {
        self.into_iter().map(|user| user.display()).collect()
    }
}

impl Display for User {
    type Output = UserDisplay;

    fn display(&self) -> UserDisplay {
        UserDisplay {
            id: self.id,
            username: self.username.clone(),
            email: self.email.clone(),
            active: self.active,
            created_at: self.created_at,
            updated_at: self.updated_at,
        }
    }
}

impl UserValidation for NewUser {
    fn validate_password(&self, pass: String) -> Result<()> {
        let re = Regex::new(PASS_REGEX).unwrap();
        if re.is_match(&pass.to_owned()) {
            return Ok(());
        }
        Err(InvalidPasswordError.into())
    }

    fn validate_email(&self, email: String) -> Result<()> {
        let re = Regex::new(EMAIL_REGEX).unwrap();
        if re.is_match(&email.to_owned()) {
            return Ok(());
        }
        Err(InvalidEmailError.into())
    }

    fn validate(&self) -> Result<()> {
        let email = self.email.to_owned();
        let pass = self.password.to_owned();
        self.validate_email(email)?;
        self.validate_password(pass)?;
        Ok(())
    }
}

impl UserValidation for User {
    fn validate_password(&self, pass: String) -> Result<()> {
        if self.password.eq(&pass) {
            return Err(SamePasswordError.into());
        }

        let re = Regex::new(PASS_REGEX).unwrap();
        if re.is_match(&pass.to_owned()) {
            return Ok(());
        }
        Err(InvalidPasswordError.into())
    }

    fn validate_email(&self, email: String) -> Result<()> {
        if self.email.eq(&email) {
            return Err(SameEmailError.into());
        }

        let re = Regex::new(EMAIL_REGEX).unwrap();
        if re.is_match(&email.to_owned()) {
            return Ok(());
        }
        Err(InvalidEmailError.into())
    }

    fn validate(&self) -> Result<()> {
        let email = self.email.to_owned();
        let pass = self.password.to_owned();
        self.validate_email(email)?;
        self.validate_password(pass)?;
        Ok(())
    }
}

impl User {
    pub fn check_password(&self, password: String) -> bool {
        if bcrypt::verify(&password.to_owned(), &self.password.to_owned()) {
            return true;
        }
        false
    }

    pub fn hash_password(password: String) -> String {
        bcrypt::hash(String::from(password)).unwrap()
    }

    pub fn repeated_user(email: &String, username: &String, conn: &PgConnection) -> Result<()> {
        match User::get_user_by_email(email.to_owned(), conn).first() {
            Some(_) => Err(SameEmailError.into()),
            None => match User::get_user_by_username(username.to_owned(), conn).first() {
                Some(_) => Err(SameUsernameError.into()),
                None => Ok(()),
            },
        }
    }

    pub fn get_all_users(conn: &PgConnection) -> Vec<User> {
        all_users
            .order(users::id.desc())
            .load::<User>(conn)
            .expect("error!")
    }

    pub fn insert_user(user: NewUser, conn: &PgConnection) -> bool {
        diesel::insert_into(users::table)
            .values(&user)
            .execute(conn)
            .is_ok()
    }

    pub fn get_user_by_username(username: String, conn: &PgConnection) -> Vec<User> {
        all_users
            .filter(users::username.eq(username))
            .load::<User>(conn)
            .expect("error!")
    }

    pub fn get_user_by_id(id: i32, conn: &PgConnection) -> Vec<User> {
        all_users
            .filter(users::id.eq(id))
            .load::<User>(conn)
            .expect("error!")
    }

    pub fn get_user_by_email(email: String, conn: &PgConnection) -> Vec<User> {
        all_users
            .filter(users::email.eq(email))
            .load::<User>(conn)
            .expect("error!")
    }
}
