table! {
    posts (id) {
        id -> Int4,
        title -> Varchar,
        body -> Text,
        published -> Bool,
        created_at -> Timestamp,
        updated_at -> Timestamp,
        deleted_at -> Nullable<Timestamp>,
        tag -> Nullable<Varchar>,
    }
}

table! {
    tag_posts (tag_id, posts_id) {
        tag_id -> Int4,
        posts_id -> Int4,
    }
}

table! {
    tags (id) {
        id -> Int4,
        title -> Varchar,
    }
}

table! {
    technologies (id) {
        id -> Int4,
        name -> Varchar,
        description -> Text,
        url -> Nullable<Varchar>,
        created_at -> Timestamp,
        updated_at -> Timestamp,
        deleted_at -> Nullable<Timestamp>,
        img -> Nullable<Varchar>,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Varchar,
        email -> Varchar,
        password -> Varchar,
        active -> Bool,
        created_at -> Timestamp,
        updated_at -> Timestamp,
        deleted_at -> Nullable<Timestamp>,
    }
}

joinable!(tag_posts -> posts (posts_id));
joinable!(tag_posts -> tags (tag_id));

allow_tables_to_appear_in_same_query!(
    posts,
    tag_posts,
    tags,
    technologies,
    users,
);
