use std::env;
use lettre_email::EmailBuilder;
use lettre::Transport;
use lettre::stub::StubTransport;
use std::error::Error;

const KEY: &str = "EMAIL";

type Result<T> = std::result::Result<T, Box<dyn Error>>;

pub fn send_email(from: String, subject: String, text: String) {

    let to = env::var(KEY).unwrap();

    let email = EmailBuilder::new()
        .to(to)
        .from(from.to_owned())
        .subject(subject.to_owned())
        .text(text.to_owned())
        .build()
        .unwrap();

    let mut mailer = StubTransport::new_positive();
    let result = mailer.send(email.into());
    println!("{:?}", result);
}
