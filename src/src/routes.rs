use rocket::*;
use rocket::response::NamedFile;
use rocket_contrib::json::Json;

use std::path::{PathBuf, Path};
use std::io::Error;

use rocket::response::content;

use super::db::database::*;
use super::models::user::NewUser;
use super::models::technology::NewTechnology;
use super::models::post::NewPost;
use super::controllers::{users, auth, base, posts, technologies};

#[get("/users", format = "application/json")]
pub fn get_all(conn: Conn, _key: auth::AuthToken) -> base::ApiResponse {
    let controller = users::UserController{conn: conn};
    controller.get_all()
}

#[post("/users", format = "application/json", data = "<new_user>")]
pub fn new_user(conn: Conn, new_user: Json<NewUser>) -> base::ApiResponse {
    let controller = users::UserController{conn: conn};
    controller.insert(new_user)
}

#[get("/users/<id>", format = "application/json")]
pub fn find_user(id: i32, conn: Conn, _key: auth::AuthToken) -> base::ApiResponse {
    let controller = users::UserController{conn: conn};
    controller.find(id)
}

#[get("/users/username/<username>", format = "application/json")]
pub fn find_user_by_username(username: String, conn: Conn, _key: auth::AuthToken) -> base::ApiResponse {
    let controller = users::UserController{conn: conn};
    controller.find_by_username(username)
}

#[post("/login", format = "application/json", data = "<login_request>")]
pub fn login(conn: Conn, login_request: Json<auth::LoginRequest>) -> base::ApiResponse {
    let controller = auth::AuthController{conn: conn};
    controller.authenticate(login_request)
}

#[get("/posts", format = "application/json")]
pub fn get_all_posts(conn: Conn) -> base::ApiResponse {
    let controller = posts::PostController{conn: conn};
    controller.get_all_posts()
}

#[post("/posts", format = "application/json", data = "<new_post>")]
pub fn new_post(conn: Conn,
                new_post: Json<NewPost>,
                _key: auth::AuthToken) -> base::ApiResponse {
    let controller = posts::PostController{conn: conn};
    controller.insert_post(new_post)
}

#[get("/posts/<title>", format = "application/json")]
pub fn get_post_by_title(title: String, conn: Conn) -> base::ApiResponse {
    let controller = posts::PostController{conn: conn};
    println!("{}", title);
    controller.find_by_title(title)
}

#[get("/techs", format = "application/json")]
pub fn get_all_techs(conn: Conn) -> base::ApiResponse {
    let controller = technologies::TechController{conn: conn};
    controller.get_all_techs()
}

#[post("/techs", format = "application/json", data = "<new_tech>")]
pub fn new_tech(conn: Conn,
                new_tech: Json<NewTechnology>) -> base::ApiResponse {
    let controller = technologies::TechController{conn: conn};
    controller.insert_tech(new_tech)
}

#[delete("/techs/<id>")]
pub fn delete_tech(id: i32,
                   conn: Conn,
                   _key: auth::AuthToken) -> base::ApiResponse {
    let controller = technologies::TechController{conn: conn};
    controller.delete_tech(id)
}

#[get("/static/<file..>")]
pub fn static_content(file: PathBuf) -> Result<NamedFile, Error> {
    NamedFile::open(Path::new("static/").join(file))
}
