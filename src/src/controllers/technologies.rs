use super::super::models::technology::{Technology, NewTechnology};
use rocket_contrib::json::Json;
use rocket::http::Status;
use super::base::{BaseController, ApiResponse};

pub type TechController = BaseController;

impl TechController {
    pub fn get_all_techs(self) -> ApiResponse {
        let techs = Technology::get_all_techs(&self.conn);
        if techs.len() == 0 {
            ApiResponse {
                status: Status::NotFound,
                json: json!({
                    "technologies": techs,
                })
            }
        } else {
            ApiResponse {
                status: Status::Ok,
                json: json!({
                    "technologies": techs,
                })
            }
        }
    }

    pub fn insert_tech(self, new_tech: Json<NewTechnology>) -> ApiResponse {
        let tech = new_tech.into_inner();

        if Technology::insert_tech(tech, &self.conn) {
            ApiResponse {
                status: Status::Created,
                json: json!({
                    "technology": Technology::get_all_techs(&self.conn)
                        .first(),
                })
            }
        } else {
            ApiResponse {
                status: Status::BadRequest,
                json: json!("Invalid Request"),
            }
        }
    }

    pub fn delete_tech(self, id: i32) -> ApiResponse {
        if id > 0 {
            let result = Technology::delete_tech(id, &self.conn);
            if result == 0 {
                ApiResponse {
                    status: Status::NotFound,
                    json: json!({"error": "No technology found"}),
                }
            } else {
                ApiResponse {
                    status: Status::Ok,
                    json: json!({"result": "Technology deleted"})
                }
            }
        } else {
            ApiResponse {
                status: Status::BadRequest,
                json: json!({ "error": "Wrong id" }),
            }
        }
    }
}
