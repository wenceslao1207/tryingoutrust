use super::super::models::post::{Post, NewPost};
use rocket_contrib::json::Json;
use rocket::http::Status;
use super::base::{BaseController, ApiResponse};

pub type PostController = BaseController;

impl PostController {
    pub fn get_all_posts(self) -> ApiResponse {
        let posts = Post::get_all_posts(&self.conn);
        if posts.len() == 0 {
            ApiResponse {
                status: Status::NotFound,
                json: json!({
                    "posts": posts,
                })
            }
        } else {
            ApiResponse {
                status: Status::Ok,
                json: json!({
                    "posts": posts,
                })
            }
        }
    }

    pub fn insert_post(self, new_post: Json<NewPost>) -> ApiResponse {
        let post = new_post.into_inner();

        if Post::insert_post(post, &self.conn) {
            ApiResponse {
                status: Status::Created,
                json: json!({
                    "post": Post::get_all_posts(&self.conn).first(),
                })
            }
        } else {
            ApiResponse {
                status: Status::BadRequest,
                json: json!("Invalid Request"),
            }
        }
    }

    pub fn find_by_title(self, title: String) -> ApiResponse {
        match Post::get_post_by_title(title, &self.conn)
            .first() {
            Some(post) => ApiResponse {
                status: Status::Ok,
                json: json!({
                    "post": post,
                })
            },
            None => ApiResponse {
                status: Status::NotFound,
                json: json!({
                    "error": "Not Found",
                })
            }
        }

    }
}

