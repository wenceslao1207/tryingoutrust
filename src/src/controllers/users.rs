use super::super::models::user::{Display, NewUser, User, UserValidation};

use super::base::{ApiResponse, BaseController};
use rocket::http::Status;
use rocket_contrib::json::Json;
use std::panic;

pub type UserController = BaseController;

impl UserController {
    pub fn get_all(self) -> ApiResponse {
        let users = User::get_all_users(&self.conn);
        if users.len() == 0 {
            return ApiResponse {
                status: Status::NotFound,
                json: json!({
                    "users": users.display(),
                }),
            };
        }
        ApiResponse {
            status: Status::Ok,
            json: json!({
                "users": users.display(),
            }),
        }
    }

    pub fn find(self, id: i32) -> ApiResponse {
        match User::get_user_by_id(id, &self.conn).first() {
            Some(user) => ApiResponse {
                status: Status::Ok,
                json: json!({
                    "user": user
                        .display(),
                }),
            },
            None => ApiResponse {
                status: Status::NotFound,
                json: json!({
                    "error": "Not Found",
                }),
            },
        }
    }

    pub fn find_by_username(self, username: String) -> ApiResponse {
        match User::get_user_by_username(username, &self.conn).first() {
            Some(user) => ApiResponse {
                status: Status::Ok,
                json: json!({
                    "user": user.display()
                }),
            },
            None => ApiResponse {
                status: Status::NotFound,
                json: json!({
                    "error": "Not Found",
                }),
            },
        }
    }

    pub fn insert(self, new_user: Json<NewUser>) -> ApiResponse {
        let mut user = new_user.into_inner();
        let valid = panic::catch_unwind(|| {
            user.validate().unwrap();
        });

        if valid.is_err() {
            return ApiResponse {
                status: Status::BadRequest,
                json: json!("Invalid Request"),
            };
        }

        match User::repeated_user(&user.email, &user.username, &self.conn) {
            Ok(_) => (),
            Err(e) => {
                return ApiResponse {
                    status: Status::BadRequest,
                    json: json!({ "error": Box::leak(e).to_string() }),
                }
            }
        };

        user.password = User::hash_password(user.password);

        if User::insert_user(user, &self.conn) {
            ApiResponse {
                status: Status::Created,
                json: json!({
                    "user": User::get_all_users(&self.conn).first().unwrap().display(),
                }),
            }
        } else {
            ApiResponse {
                status: Status::BadRequest,
                json: json!("Invalid Request"),
            }
        }
    }
}
