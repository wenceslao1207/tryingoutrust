use serde::Deserialize;
use rocket::Outcome;
use rocket::http::Status;
use rocket_contrib::json::Json;
use rocket::request::{self, Request, FromRequest};

use super::super::models::user::User;
use super::super::auth::token;
use super::base::ApiResponse;
use super::base::BaseController;

pub type AuthController = BaseController;

#[derive(Deserialize)]
pub struct LoginRequest {
    pub email: String,
    pub password: String,
}

pub struct AuthToken(String);

#[derive(Debug)]
pub enum AuthHeaderError {
    Missing,
    Invalid,
    BadCount,
}

impl AuthController {
    pub fn authenticate(self, login_request: Json<LoginRequest>) -> ApiResponse {
        let request = login_request.into_inner();
        match User::get_user_by_email(request.email, &self.conn)
            .first() {
            Some(user) => {
                if user.check_password(request.password) {
                    match token::Claims::generate_token(&user) {
                        Ok(token) => ApiResponse{
                            status: Status::Ok,
                            json: json!({
                                "user": user.username,
                                "token": token.to_owned(),
                                "roles": ["ROLE_ADMIN"],
                            }),
                        },
                        Err(err) => ApiResponse {
                            status: Status::Unauthorized,
                            json: json!({
                                "error": *err.to_string(),
                            }),
                        },
                    }
                } else {
                   let err = json!({
                       "error": "Error wrong password",
                   }) ;
                   ApiResponse {
                        status: Status::Unauthorized,
                        json:(err),
                   }
                }
            },
            None => ApiResponse {
                status: Status::NotFound,
                json: json!({"error": "No user found"}),
            }
        }
    }
}

impl <'a, 'r> FromRequest<'a, 'r> for AuthToken {

    type Error = AuthHeaderError;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let keys: Vec<_> = request.headers().get("Authorization").collect();
        match keys.len() {
            0 => Outcome::Failure((Status::BadRequest, AuthHeaderError::Missing)),
            1 if is_token_valid(keys[0]) => Outcome::Success(
                AuthToken(keys[0].to_string())
                ),
            1 => Outcome::Failure((Status::BadRequest, AuthHeaderError::Invalid)),
            _ => Outcome::Failure((Status::BadRequest, AuthHeaderError::BadCount)),
        }

    }
    
}

fn is_token_valid(key: &str) -> bool {
    let split_token: Vec<&str> = key.split("Bearer ").collect();
    if split_token.len() <= 1 {
        false
    } else {
        let token = split_token[1];
        match token::Claims::validation(token.to_string()) {
            Ok(_) => true,
            Err(_) => false,
        }
    }
}
