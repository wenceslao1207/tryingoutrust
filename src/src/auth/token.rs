use rand;
use std::{env, str, iter};
use std::error::Error;
use std::convert::TryFrom;
use serde::{Serialize, Deserialize};
use rand::Rng;
use rand::distributions::Alphanumeric;
use chrono::{DateTime, Utc, Duration};
use jsonwebtoken::{
    encode,
    decode,
    Header,
    Validation,
    EncodingKey,
    DecodingKey,
    TokenData
};

use super::super::models::user::User;

const SUB: &str = "esposta@news.com";
const KEY: &str = "SECRET_KEY";

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    sub: String,
    username: String,
    exp: usize,
}


impl Claims {

    fn generate_exp() -> usize {
        let now: DateTime<Utc> = Utc::now();
        let tomorrow = now + Duration::days(1);
        usize::try_from(tomorrow.timestamp()).unwrap()
    }


    fn get_secret_key() -> Vec<u8> {
        let secret_key = match env::var(KEY) {
            Ok(value) => value.as_bytes().to_vec(),
            Err(_) => Claims::gen_random_key(),
        };
        Claims::store_secret_key(&secret_key);
        secret_key
    }

    fn gen_random_key() -> Vec<u8> {
        let mut rng = rand::thread_rng();
        let random_string: String = iter::repeat(())
                .map(|()| rng.sample(Alphanumeric))
                .take(7)
                .collect();
        random_string.as_bytes().to_vec()
    }

    fn store_secret_key(secret: &Vec<u8>) {
        let value = str::from_utf8(secret).unwrap();
        env::set_var(KEY, value);
    }

    pub fn generate_token(user: &User) -> Result<String, Box<dyn Error>> {
        let sub = SUB.to_owned();
        let key = Claims::get_secret_key();
        let username = user.username.to_owned();

        let user_claims = 
            Claims { sub: sub, username: username, exp: Claims::generate_exp() };
        match encode(&Header::default(),
                                 &user_claims,
                                 &EncodingKey::from_secret(&key)
                                 ) {
            Ok(token) => Ok(token),
            Err(err) => Err(err.into()),
        }
    }

    pub fn validation(token: String) -> Result<TokenData<Claims>, Box<dyn Error>> {
        let validation = Validation {
           sub: Some(SUB.to_string()),
           ..Validation::default()
        };
        let key = Claims::get_secret_key();

        match decode::<Claims>(&token,
                               &DecodingKey::from_secret(&key),
                               &validation
                               ) {
                Ok(claims) => Ok(claims),
                Err(err) => Err(Box::new(err)),
        }
    }
}

