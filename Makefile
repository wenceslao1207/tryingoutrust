up:
	podman-compose up

down:
	podman-compose down

build:
	cd src; buildah bud -t blog .

shell:
	podman exec -it blog_app_1 bash

sql:
	podman exec -it blog_db_1 psql -U blog
